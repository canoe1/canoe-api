package com.esgi.canoe.api.authentication.login;

import lombok.Data;

@Data
public class LoginDTO {

    private String email;
    private String password;
}
