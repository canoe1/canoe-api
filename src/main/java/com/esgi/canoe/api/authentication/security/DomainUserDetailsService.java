package com.esgi.canoe.api.authentication.security;

import com.esgi.canoe.api.components.user.UserRepository;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.lang.reflect.Array;

@Component
public class DomainUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    public DomainUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        com.esgi.canoe.api.components.user.User appUser = userRepository.findByEmail(email)
                .orElseThrow(() -> new AuthenticationServiceException("email " + email + " not found"));
        String[] roles = new String[]{appUser.getRole()};

        return User.builder()
                .username(email)
                .password(appUser.getPassword())
                .roles(roles)
                .accountExpired(false)
                .accountLocked(false)
                .credentialsExpired(false)
                .disabled(false)
                .build();
    }
}
