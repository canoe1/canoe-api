package com.esgi.canoe.api.authentication.login;

import com.esgi.canoe.api.components.user.CreateUserDTO;
import com.esgi.canoe.api.components.user.User;
import com.esgi.canoe.api.components.user.UserRepository;
import com.esgi.canoe.api.components.user.UserService;
import com.esgi.canoe.api.shared.services.URIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;

@RestController
@RequestMapping("/signup")
public class SignupController {
    private final UserService userService;
    private final UserRepository userRepository;
    private final URIService uriService;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public SignupController(UserService userService, UserRepository userRepository, URIService uriService, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.userRepository = userRepository;
        this.uriService = uriService;
        this.passwordEncoder = passwordEncoder;
    }

    @PostMapping
    public ResponseEntity<Void> CreateUser(@Validated @RequestBody CreateUserDTO user) {
        User u = User
                .builder()
                .email(user.getEmail())
                .password( passwordEncoder.encode(user.getPassword()))
                .firstname(user.getFirstname() != null ? user.getFirstname() : "")
                .lastname(user.getLastname() != null ? user.getLastname() : "")
                .role("USER")
                .build();

        u = this.userRepository.save(u);

        URI location = this.uriService.getUser(u.getUserId());

        return ResponseEntity.created(location).build();
    }
}
