package com.esgi.canoe.api.components.user;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CreateUserDTO {

    @NotNull
    public String email;
    @NotNull
    public String password;

    public String firstname;

    public String lastname;


}
