package com.esgi.canoe.api.components.search;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Getter
@Setter
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name="searches")
public class Search {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer searchId;

    @NotNull
    private String product_name;

    @NotNull
    private String basket;

    @NotNull
    private String file_path;

    @NotNull
    private String type;

    @NotNull
    private String token;

    @NotNull
    private float percentage;

    @NotNull
    private String status;

    @NotNull
    @Column(columnDefinition="TEXT")
    private String searchResult;

}
