package com.esgi.canoe.api.components.follow;

import org.springframework.stereotype.Component;

import java.util.TimerTask;

@Component
public class TimerProductInformation extends TimerTask {
    private final FollowService followService;

    public TimerProductInformation(FollowService followService) {
        this.followService = followService;
    }

    @Override
    public void run() {
        try{
            this.followService.runBackgroundProductBrowser();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
