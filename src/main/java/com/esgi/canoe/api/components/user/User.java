package com.esgi.canoe.api.components.user;

import com.esgi.canoe.api.components.favorite.Favorite;
import lombok.*;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
@Getter
@Setter
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name="users")
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer userId;

    @NotNull
    private String email;

    private String token;

    @NotNull
    private String password;

    @NotNull
    private String role;

    private String firstname;
    private String lastname;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Favorite> favorites;

    @Override
    public String toString() {
        return this.firstname + " " + this.lastname + ": " + this.email;
    }
}
