package com.esgi.canoe.api.components.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GetUserDTO {
    public String email;
    public String password;

    public String role;

    public String firstname;
    public String lastname;
}
