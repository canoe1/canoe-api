package com.esgi.canoe.api.components.search;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GetSearchDTO {

    private String product_name;
    private String type;
    private String searchResult;
    private String file_path;
    private String basket;
}
