package com.esgi.canoe.api.components.search;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.criteria.CriteriaBuilder;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class PostSearchDTO {
    public String type;

    public String product_name;

    public String basket;

    public String file_path;

    public Boolean enable_aliExpress;

    public PostSearchDTO(String dto, String byteCode) {

        dto = dto.substring(1, dto.length()-1);
        String[] keyValuePairs = dto.split(",");
        Map<String,String> map = new HashMap<>();

        for(String pair : keyValuePairs)
        {
            String[] entry = pair.split(":");
            map.put(entry[0].trim(), entry[1].trim());
        }

        this.type = map.get("\"type\"").replace("\"", "");
        this.product_name = map.get("\"product_name\"").replace("\"", "");
        this.basket = map.get("\"basket\"").replace("\"", "");
        this.file_path = byteCode;
        this.enable_aliExpress = Boolean.valueOf(map.get("\"enable_aliExpress\"").replace("\"", ""));
    }

    public String toStringAllParams(String token){
        return
                "{\n" +
                        "\"type\": \""+this.type+"\",\n" +
                        "\"product_name\" : \""+this.product_name+"\",\n" +
                        "\"basket\" : \""+this.basket+"\",\n" +
                        "\"token\" : \""+token+"\",\n" +
                        "\"file_path\" : \""+this.file_path+"\",\n"+
                        "\"enable_aliExpress\" : \""+this.enable_aliExpress.toString()+"\",\n" +
                        "}";
    }


    @Override
    public String toString() {
        return
                "{\n" +
                    "\"type\": \""+this.type+"\",\n" +
                    "\"product_name\" : \""+this.product_name+"\",\n" +
                    "\"enable_aliExpress\" : \""+this.enable_aliExpress.toString()+"\",\n" +
                "}";
    }
}
