package com.esgi.canoe.api.components.broker;

import com.esgi.canoe.MainApplication;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class Runner{

    private final RabbitTemplate rabbitTemplate;
    private final MessageReceiver receiver;

    public void setJson(String json) {
        this.json = json;
    }

    private String json;

    public Runner(MessageReceiver messageReceiver, RabbitTemplate template) {
        this.receiver = messageReceiver;
        this.rabbitTemplate = template;
    }


    public void runSearch() throws Exception{
        System.out.println("Sending message...");
        rabbitTemplate.convertAndSend("to_python_queue", json);
        System.out.println("Sending done ?");
    }
}
