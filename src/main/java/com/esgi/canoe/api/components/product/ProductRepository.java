package com.esgi.canoe.api.components.product;


import com.esgi.canoe.api.components.favorite.Favorite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;


@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {

    Product findByUrl(String token);

    List<Product> getAllByFavoritesIsIn(Collection<List<Favorite>> favorites);

    List<Product> findAll();
}
