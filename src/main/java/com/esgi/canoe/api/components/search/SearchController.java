package com.esgi.canoe.api.components.search;

import com.esgi.canoe.api.components.broker.Runner;
import com.esgi.canoe.api.components.favorite.Favorite;
import com.esgi.canoe.api.components.favorite.FavoriteRepository;
import com.esgi.canoe.api.components.follow.Follow;
import com.esgi.canoe.api.components.follow.FollowRepository;
import com.esgi.canoe.api.components.user.User;
import com.esgi.canoe.api.components.user.UserRepository;
import com.esgi.canoe.api.components.user.UserService;
import org.springframework.http.MediaType;
import org.springframework.boot.configurationprocessor.json.JSONArray;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.net.URISyntaxException;
import java.util.List;


@RestController
@RequestMapping("/search")
public class SearchController {
    private final UserService userService;
    private final Runner runner;
    private final SearchRepository searchRepository;
    private final SearchService searchService;
    private final UserRepository userRepository;
    private final FavoriteRepository favoriteRepository;
    private final FollowRepository followRepository;


    public SearchController(UserService userService, Runner runner, SearchRepository searchRepository,
                            SearchRepository searchRepository1, SearchService searchService, UserRepository userRepository, FavoriteRepository favoriteRepository, FollowRepository followRepository) {
        this.userService = userService;
        this.runner = runner;
        this.searchRepository = searchRepository1;
        this.searchService = searchService;
        this.userRepository = userRepository;
        this.favoriteRepository = favoriteRepository;
        this.followRepository = followRepository;
    }

    @PostMapping
    public ResponseEntity<Void> postSearch(@Validated @RequestBody PostSearchDTO postSearchDTO,
                                           @RequestHeader("Authorization") String authKey){
        String token = authKey.split(" ")[1];
        System.out.println(postSearchDTO.toString());

        Search previousSearch = this.searchRepository.findByTokenAndStatus(token, "ongoing");

        if(previousSearch != null){
            previousSearch.setStatus("cancelled");
            this.searchRepository.save(previousSearch);
        }

        previousSearch = this.searchRepository.findByTokenAndStatus(token, "Done");

        if(previousSearch != null){
            previousSearch.setStatus("cancelled");
            previousSearch.setToken("");
            previousSearch.setSearchResult("");
            this.searchRepository.save(previousSearch);
        }

        try {
            this.runner.setJson(postSearchDTO.toStringAllParams(token));
            this.runner.runSearch();

            Search search = Search.builder()
                    .token(token)
                    .type(postSearchDTO.type)
                    .product_name(postSearchDTO.product_name)
                    .searchResult("")
                    .percentage(0)
                    .status("ongoing")
                    .file_path(postSearchDTO.file_path)
                    .basket(postSearchDTO.basket)
                    .build();

            this.searchRepository.save(search);

            return ResponseEntity.accepted().build();
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        }
    }

    @GetMapping("/token")
    public GetSearchDTO getSearchResult(@RequestHeader("Authorization") String authKey){
        String token = authKey.split(" ")[1];

        User user = this.userRepository.findByToken(token)
                .orElseThrow(() -> new AuthenticationServiceException("token " + token + " not found"));

        List<Favorite> favorites = this.favoriteRepository.findAllByUser(user);

        List<Follow> follows = this.followRepository.findAllByUser(user);

        Search result = this.searchRepository.findByToken(token);

        if (result == null){
            return null;
        }

        GetSearchDTO resultDto = this.searchService.getSearchDTO(result);

        if (!result.getSearchResult().equals("")){
            JSONArray searchResultObject;
            JSONObject searchResult = new JSONObject();
            try {
                if(!result.getType().equals("basket")){
                    searchResultObject = new JSONArray(result.getSearchResult());
                }else{
                    searchResult =  new JSONObject(result.getSearchResult());
                    searchResultObject = searchResult.getJSONArray("products");
                }

                for(int i = 0 ; i < searchResultObject.length(); i++){
                    JSONObject row = searchResultObject.getJSONObject(i);
                    for(Favorite favorite : favorites){
                        if( row.getString("url").equals(favorite.getProduct().getUrl())){
                            searchResultObject.getJSONObject(i).put("user_rating",favorite.getRatings());
                        }
                    }
                    if(!searchResultObject.getJSONObject(i).has("user_rating")){
                        searchResultObject.getJSONObject(i).put("user_rating",-1);
                    }

                    for(Follow follow : follows){
                        if( row.getString("url").equals(follow.getProduct().getUrl())){
                            searchResultObject.getJSONObject(i).put("price_notify",follow.getPrice_notify());
                        }
                    }

                    if(!searchResultObject.getJSONObject(i).has("price_notify")){
                        searchResultObject.getJSONObject(i).put("price_notify",-1);
                    }
                }

                if(!result.getType().equals("basket")){
                    result.setSearchResult(searchResultObject.toString());
                }else{
                    searchResult.put("products", searchResultObject);
                    result.setSearchResult(searchResult.toString());
                }



            } catch (JSONException e) {
                e.printStackTrace();
            }

            resultDto = this.searchService.getSearchDTO(result);

            result.setSearchResult("");
            result.setToken("");

            this.searchRepository.save(result);
        }
        return resultDto;
    }

    @GetMapping("/percentage")
    public GetSearchPercentageDTO getSearchPercentageDTO(@RequestHeader("Authorization") String authKey) {
        String token = authKey.split(" ")[1];

        Search search = this.searchRepository.findByTokenAndStatusNot(token, "Done");

        if(search == null){
            search = this.searchRepository.findByTokenAndStatusNot(token, "ongoing");
        }

        if(search != null){
            return GetSearchPercentageDTO.builder()
                    .percentage(search.getPercentage())
                    .build();
        }else{
            return null;
        }
    }

    @PutMapping("/percentage")
    public ResponseEntity<Void> updateSearchPercentage(@RequestHeader("Authorization") String authKey,
                                                       @Validated @RequestBody PutSearchPercentageDTO putSearchPercentageDTO){
        String token = authKey.split(" ")[1];


        try {
            Search search = this.searchRepository.findByTokenAndStatus(token, "ongoing");
            float value = search.getPercentage() + putSearchPercentageDTO.getPercentage() == 100 ? (float) 99.99 : search.getPercentage() + putSearchPercentageDTO.getPercentage();
            search.setPercentage(value);

            this.searchRepository.save(search);

            return ResponseEntity.accepted().build();
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        }
    }
}
