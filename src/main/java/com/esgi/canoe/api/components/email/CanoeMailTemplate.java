package com.esgi.canoe.api.components.email;

import com.esgi.canoe.api.components.product.Product;
import com.esgi.canoe.api.components.user.User;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.stereotype.Service;

@Service
public class CanoeMailTemplate {

    public String getFollowProductMailBody(JSONObject product) throws JSONException {
        return "Alerte baisse de prix !\n\n" +
                "Le produit "+product.getString("name")+" est disponible pour "+String.valueOf(product.getDouble("price"))+ " €.\n" +
                "Il est disponible sur le lien suivant :"+product.getString("url")+"\n" +
                "Cordialement,\n" +
                "L'équipe Canoe.";
    }
}
