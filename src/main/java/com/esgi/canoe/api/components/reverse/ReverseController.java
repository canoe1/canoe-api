package com.esgi.canoe.api.components.reverse;

import com.esgi.canoe.api.components.broker.Runner;
import com.esgi.canoe.api.components.search.PostSearchDTO;
import com.esgi.canoe.api.components.search.Search;
import com.esgi.canoe.api.components.search.SearchRepository;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.file.Files;
import java.util.Arrays;

@RestController
@RequestMapping("/reverse")
public class ReverseController {

    private final ReverseService reverseService;
    private final Runner runner;
    private final SearchRepository searchRepository;
    private Integer searchId;

    public ReverseController(ReverseService reverseService, Runner runner, SearchRepository searchRepository) {
        this.reverseService = reverseService;
        this.runner = runner;
        this.searchRepository =searchRepository;
    }

    @GetMapping
    public ResponseEntity<byte[]> getImageAsResponseEntity() throws IOException {
        HttpHeaders headers = new HttpHeaders();
        File imgPath = new File(System.getProperty("user.dir") + "\\output.png");
        byte[] fileContent = Files.readAllBytes(imgPath.toPath());
        headers.setCacheControl(CacheControl.noCache().getHeaderValue());

        ResponseEntity<byte[]> responseEntity = new ResponseEntity<>(fileContent, headers, HttpStatus.OK);
        imgPath.delete();

        return responseEntity;
    }

    @PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.IMAGE_PNG_VALUE)
    public ResponseEntity<Void> postReverse(
            @RequestParam(value = "image", required = true) MultipartFile image,
            @RequestHeader("Authorization") String authKey,
            @RequestParam String postSearchDTO

    ) throws Exception {
        File imageFile = this.reverseService.saveFile(image);

        String imagePath = this.reverseService.convertToImage(imageFile);
        String byteCodeList = Arrays.toString(image.getBytes());
        String token = authKey.split(" ")[1];

        var mapedPostSearchDTO = new PostSearchDTO(postSearchDTO, byteCodeList);

        try {
            this.runner.setJson(mapedPostSearchDTO.toStringAllParams(token));
            this.runner.runSearch();

            Search search = Search.builder()
                    .token(token)
                    .type(mapedPostSearchDTO.type)
                    .product_name(mapedPostSearchDTO.product_name)
                    .searchResult("")
                    .file_path(imagePath)
                    .percentage(0)
                    .status("ongoing")
                    .basket(mapedPostSearchDTO.basket)
                    .build();

            Search savedSearch = this.searchRepository.save(search);
            Integer savedSearchId = savedSearch.getSearchId();
            this.searchId = savedSearchId;

            return ResponseEntity.accepted().build();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("server error");
            return ResponseEntity.badRequest().build();
        }
    }

    @PostMapping(value = "/keyWord")
    public ResponseEntity<Void> postReverseKeyWord(
            @RequestHeader("Authorization") String authKey,
            @RequestParam String keyWord
    ) throws Exception {

        try {
            Search searchToUpdate = this.searchRepository.getOne(this.searchId);
            searchToUpdate.setProduct_name(keyWord);
            this.searchRepository.save(searchToUpdate);
            return ResponseEntity.accepted().build();

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("server error");
            return ResponseEntity.badRequest().build();
        }
    }

    @GetMapping(value = "/keyWord")
    public ResponseEntity<String> getProduct_name() throws IOException {
        var productName = this.searchRepository.getOne(this.searchId).getProduct_name();
        return ResponseEntity.ok(productName);
    }
}
