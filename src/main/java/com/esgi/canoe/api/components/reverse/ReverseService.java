package com.esgi.canoe.api.components.reverse;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;

@Service
public class ReverseService {

    public String convertToImage(File file) throws IOException {
        String path = System.getProperty("user.dir") + "\\output.png";
        BufferedImage bimage = ImageIO.read(file);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ImageIO.write(bimage, "png", bos);
        byte[] data = bos.toByteArray();
        ByteArrayInputStream bis = new ByteArrayInputStream(data);
        BufferedImage bImage2 = ImageIO.read(bis);
        ImageIO.write(bImage2, "png", new File(path));
        return path;
    }

    public File saveFile(MultipartFile image) throws Exception {
        File imageFile = new File(System.getProperty("user.dir") + "\\output.imgBytes");
        OutputStream out = new FileOutputStream(imageFile);
        out.write(image.getBytes());
        out.close();
        return imageFile;
    }
}
