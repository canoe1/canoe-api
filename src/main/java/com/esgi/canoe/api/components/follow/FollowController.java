package com.esgi.canoe.api.components.follow;

import com.esgi.canoe.api.components.email.CanoeMailSender;
import com.esgi.canoe.api.components.product.ProductRepository;
import com.esgi.canoe.api.components.search.Search;
import com.esgi.canoe.api.components.search.SearchRepository;
import com.esgi.canoe.api.components.user.User;
import com.esgi.canoe.api.components.user.UserRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.GregorianCalendar;
import java.util.List;
import java.util.Timer;

@RestController
@RequestMapping("/follow")
public class FollowController {
    private final FollowService followService;
    private final SearchRepository searchRepository;
    private final UserRepository userRepository;
    private final FollowRepository followRepository;
    private final ProductRepository productRepository;
    private final CanoeMailSender canoeMailSender;

    public FollowController(FollowService followService, SearchRepository searchRepository, UserRepository userRepository, FollowRepository followRepository, ProductRepository productRepository, CanoeMailSender canoeMailSender) {
        this.followService = followService;
        this.searchRepository = searchRepository;
        this.userRepository = userRepository;
        this.followRepository = followRepository;
        this.productRepository = productRepository;
        this.canoeMailSender = canoeMailSender;
    }

    @PostMapping
    public ResponseEntity<Void> postSearch(@Validated @RequestBody PostFollowDTO postFollowDTO){
        try {
            this.followService.postFollow(postFollowDTO);
            return ResponseEntity.accepted().build();
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        }
    }

    @PostMapping("/email")
    public ResponseEntity<Void> getFollowsByEmail(@RequestHeader("Authorization") String authKey){
        String token = authKey.split(" ")[1];

        Search previousSearch = this.searchRepository.findByTokenAndStatus(token, "ongoing");

        if(previousSearch != null){
            previousSearch.setStatus("cancelled");
            this.searchRepository.save(previousSearch);
        }


        try {
            this.followService.launchGetFollowProcess(token);

            return ResponseEntity.accepted().build();
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        }
    }

    @PostMapping("/delete")
    public ResponseEntity<Void> deleteFavorite(@Validated @RequestBody DeleteFollowDTO deleteFollowDTO,
                                               @RequestHeader("Authorization") String authKey){
        try {
            String token = authKey.split(" ")[1];

            User user = this.userRepository.findByToken(token)
                    .orElseThrow(() -> new AuthenticationServiceException("token " + token + " not found"));


            List<Follow> follows = this.followRepository.findAllByUser(user);

            for(Follow follow : follows){
                if(follow.getProduct().getUrl().equals(deleteFollowDTO.url)){
                    this.followRepository.delete(follow);
                    break;
                }
            }

            return ResponseEntity.accepted().build();
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        }
    }

    @PostMapping("/scheduler/start")
    public ResponseEntity<Void> startScheduler(){
        try {

            Timer timer = new Timer();
            GregorianCalendar gc = new GregorianCalendar();
            timer.scheduleAtFixedRate(new TimerProductInformation(followService), gc.getTime(), 300000);

            return ResponseEntity.accepted().build();
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        }
    }

}
