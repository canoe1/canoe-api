package com.esgi.canoe.api.components.flight;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class PostFlightDTO {
    @NotNull
    String type;

    @NotNull
    String origin_airport;

    @NotNull
    String destination_airport;

    @NotNull
    String departure_date;

    @NotNull
    float passenger;

    public String toStringAllParams(String token){
        return
                "{\n" +
                        "\"type\": \""+this.type+"\",\n" +
                        "\"origin_airport\" : \""+this.origin_airport+"\",\n" +
                        "\"destination_airport\" : \""+this.destination_airport+"\",\n" +
                        "\"token\" : \""+token+"\",\n" +
                        "\"departure_date\" : \""+this.departure_date+"\",\n"+
                        "\"passenger\" : "+this.passenger+",\n" +
                        "}";
    }
}
