package com.esgi.canoe.api.components.follow;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class PostFollowDTO {
    String email;

    String product_url;

    Float price_notify;
}
