package com.esgi.canoe.api.components.search;

import org.springframework.stereotype.Service;

@Service
public class SearchService {
    public GetSearchDTO getSearchDTO(Search searchResult) {
        return GetSearchDTO.builder()
                .product_name(searchResult.getProduct_name())
                .basket(searchResult.getBasket())
                .file_path(searchResult.getFile_path())
                .type(searchResult.getType())
                .searchResult(searchResult.getSearchResult())
                .build();
    }
}
