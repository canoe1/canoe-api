package com.esgi.canoe.api.components.search;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface SearchRepository extends JpaRepository<Search, Integer> {

    Search findByToken(String token);
    Search findByTokenAndStatus(String token, String status);
    Search findByTokenAndStatusNot(String token, String status);
    List<Search> findAllByTokenAndStatus(String token, String status);
}
