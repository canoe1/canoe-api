package com.esgi.canoe.api.components.favorite;

import com.esgi.canoe.api.components.product.Product;
import com.esgi.canoe.api.components.product.ProductRepository;
import com.esgi.canoe.api.components.search.GetSearchDTO;
import com.esgi.canoe.api.components.search.Search;
import com.esgi.canoe.api.components.search.SearchRepository;
import com.esgi.canoe.api.components.search.SearchService;
import com.esgi.canoe.api.components.user.User;
import com.esgi.canoe.api.components.user.UserRepository;
import org.springframework.boot.configurationprocessor.json.JSONArray;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/favorite")
public class FavoriteController {
    private final FavoriteService favoriteService;
    private final SearchRepository searchRepository;
    private final SearchService searchService;
    private final UserRepository userRepository;
    private final FavoriteRepository favoriteRepository;
    private final ProductRepository productRepository;



    public FavoriteController(FavoriteService favoriteService, SearchRepository searchRepository, SearchService searchService, UserRepository userRepository, FavoriteRepository favoriteRepository, ProductRepository productRepository) {
        this.favoriteService = favoriteService;
        this.searchRepository = searchRepository;
        this.searchService = searchService;
        this.userRepository = userRepository;
        this.favoriteRepository = favoriteRepository;
        this.productRepository = productRepository;
    }

    @PostMapping
    public ResponseEntity<Void> postSearch(@Validated @RequestBody PostFavoriteDTO postFavoriteDTO){
        try {
            this.favoriteService.postFavorite(postFavoriteDTO);
            return ResponseEntity.accepted().build();
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        }
    }

    @PostMapping("/email")
    public ResponseEntity<Void> getFavoritesByEmail(@RequestHeader("Authorization") String authKey){
        String token = authKey.split(" ")[1];

        Search previousSearch = this.searchRepository.findByTokenAndStatus(token, "ongoing");

        if(previousSearch != null){
            previousSearch.setStatus("cancelled");
            this.searchRepository.save(previousSearch);
        }


        try {
            this.favoriteService.launchGetFavoriteProcess(token);

            return ResponseEntity.accepted().build();
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        }
    }

    @PostMapping("/delete")
    public ResponseEntity<Void> deleteFavorite(@Validated @RequestBody DeleteFavoriteDTO deleteFavoriteDTO,
                                               @RequestHeader("Authorization") String authKey){

        String token = authKey.split(" ")[1];

        User user = this.userRepository.findByToken(token)
                .orElseThrow(() -> new AuthenticationServiceException("token " + token + " not found"));


        List<Favorite> favorites = this.favoriteRepository.findAllByUser(user);

        for(Favorite favorite : favorites){
            if(favorite.getProduct().getUrl().equals(deleteFavoriteDTO.url)){
                this.favoriteRepository.delete(favorite);
                break;
            }
        }

        try {
            return ResponseEntity.accepted().build();
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        }
    }

}
