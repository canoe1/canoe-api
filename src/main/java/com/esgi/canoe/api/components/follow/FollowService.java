package com.esgi.canoe.api.components.follow;

import com.esgi.canoe.api.components.broker.Runner;
import com.esgi.canoe.api.components.email.CanoeMailSender;
import com.esgi.canoe.api.components.favorite.FavoriteRepository;
import com.esgi.canoe.api.components.favorite.LaunchFavoriteDTO;
import com.esgi.canoe.api.components.product.Product;
import com.esgi.canoe.api.components.product.ProductRepository;
import com.esgi.canoe.api.components.search.Search;
import com.esgi.canoe.api.components.search.SearchRepository;
import com.esgi.canoe.api.components.user.User;
import com.esgi.canoe.api.components.user.UserRepository;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FollowService {
    private final UserRepository userRepository;
    private final ProductRepository productRepository;
    private final FollowRepository followRepository;
    private final Runner runner;
    private final SearchRepository searchRepository;
    private final FavoriteRepository favoriteRepository;
    private final CanoeMailSender canoeMailSender;

    public FollowService(UserRepository userRepository, ProductRepository productRepository, FollowRepository followRepository, Runner runner, SearchRepository searchRepository, FavoriteRepository favoriteRepository, CanoeMailSender canoeMailSender) {
        this.userRepository = userRepository;
        this.productRepository = productRepository;
        this.followRepository = followRepository;
        this.runner = runner;
        this.searchRepository = searchRepository;
        this.favoriteRepository = favoriteRepository;
        this.canoeMailSender = canoeMailSender;
    }

    public void postFollow(PostFollowDTO postFollowDTO){
        try {

            User user = this.userRepository.findByEmail(postFollowDTO.email)
                    .orElseThrow(() -> new AuthenticationServiceException("email " + postFollowDTO.email + " not found"));

            Product product = this.productRepository.findByUrl(postFollowDTO.product_url);

            if (product == null) {
                product = Product.builder()
                        .url(postFollowDTO.product_url)
                        .build();

                product = this.productRepository.save(product);
            }

            Follow previousFollow =  this.followRepository.findByUserAndProduct(user, product);

            if(previousFollow != null){
                previousFollow.setPrice_notify(postFollowDTO.price_notify);

                this.followRepository.save(previousFollow);
            }else{
                Follow follow = Follow.builder()
                        .price_notify(postFollowDTO.price_notify)
                        .user(user)
                        .product(product)
                        .build();

                this.followRepository.save(follow);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void launchGetFollowProcess(String token) throws Exception {
        User user = this.userRepository.findByToken(token)
                .orElseThrow(() -> new AuthenticationServiceException("token " + token + " not found"));


        List<Follow> follows = this.followRepository.findAllByUser(user);


        List<String> productUrls = new ArrayList<>();

        for (Follow follow : follows) {
            productUrls.add(follow.product.getUrl());
        }

        LaunchFavoriteDTO launchFavoriteDTO =  LaunchFavoriteDTO.builder()
                .productUrls(productUrls)
                .token(token)
                .type("favorite")
                .build();

        System.out.println(launchFavoriteDTO.toString());
        this.runner.setJson(launchFavoriteDTO.toString());
        this.runner.runSearch();

        Search search = Search.builder()
                .token(token)
                .percentage(0)
                .type("favorite")
                .product_name("")
                .searchResult("")
                .file_path("")
                .basket("")
                .status("ongoing")
                .percentage(0)
                .build();
        this.searchRepository.save(search);
    }

    public void runBackgroundProductBrowser() throws Exception {
        String token = "schedulerCanoe";

        Search previousSearch = this.searchRepository.findByTokenAndStatus(token, "ongoing");

        if(previousSearch != null){
            previousSearch.setStatus("cancelled");
            this.searchRepository.save(previousSearch);
        }

        List<Product> products = this.productRepository.findAll();

        List<String> productUrls = new ArrayList<>();

        for (Product product : products) {
            productUrls.add(product.getUrl());
        }

        LaunchFavoriteDTO launchFavoriteDTO =  LaunchFavoriteDTO.builder()
                .productUrls(productUrls)
                .token(token)
                .type("favorite")
                .build();


        this.runner.setJson(launchFavoriteDTO.toString());
        this.runner.runSearch();

        Search search = Search.builder()
                .token(token)
                .type("favorite")
                .product_name("")
                .searchResult("")
                .file_path("")
                .basket("")
                .status("ongoing")
                .percentage(0)
                .build();
        this.searchRepository.save(search);
    }


}

