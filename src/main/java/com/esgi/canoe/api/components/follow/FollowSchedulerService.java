package com.esgi.canoe.api.components.follow;

import com.esgi.canoe.api.components.email.CanoeMailSender;
import com.esgi.canoe.api.components.email.CanoeMailTemplate;
import com.esgi.canoe.api.components.search.Search;
import com.esgi.canoe.api.components.search.SearchRepository;
import org.springframework.boot.configurationprocessor.json.JSONArray;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FollowSchedulerService {
    private final  FollowRepository followRepository;
    private final SearchRepository searchRepository;
    private final CanoeMailSender canoeMailSender;
    private final CanoeMailTemplate canoeMailTemplate;

    public FollowSchedulerService(FollowRepository followRepository, SearchRepository searchRepository, CanoeMailSender canoeMailSender, CanoeMailTemplate canoeMailTemplate) {
        this.followRepository = followRepository;
        this.searchRepository = searchRepository;
        this.canoeMailSender = canoeMailSender;
        this.canoeMailTemplate = canoeMailTemplate;
    }

    public void notifyFollowingUser(){
        String token = "schedulerCanoe";


        List<Follow> follows = this.followRepository.findAll();

        Search result = this.searchRepository.findByToken(token);

        if (!result.getSearchResult().equals("")){
            try {
                JSONArray searchResultObject;

                searchResultObject = new JSONArray(result.getSearchResult());


                for(int i = 0 ; i < searchResultObject.length(); i++){
                    JSONObject row = searchResultObject.getJSONObject(i);


                    for(Follow follow : follows){
                        if( row.getString("url").equals(follow.getProduct().getUrl())){
                            if(row.getDouble("price") < follow.getPrice_notify()){
                                if(follow.getLast_price_notified() == null  || row.getDouble("price") < follow.getLast_price_notified()){
                                    this.canoeMailSender.sendSimpleMessage(follow.user.getEmail(),
                                                                        "Alerte diminution de prix",
                                                                            this.canoeMailTemplate.getFollowProductMailBody(row));
                                    follow.setLast_price_notified((float) row.getDouble("price"));
                                    this.followRepository.save(follow);
                                }
                            }
                            System.out.println("Found match : "+follow.user.getEmail());
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            result.setSearchResult("");
            result.setToken("");

            this.searchRepository.save(result);

        }

    }
}
