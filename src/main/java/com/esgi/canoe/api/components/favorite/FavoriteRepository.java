package com.esgi.canoe.api.components.favorite;

import com.esgi.canoe.api.components.product.Product;
import com.esgi.canoe.api.components.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FavoriteRepository extends JpaRepository<Favorite, Integer> {
    List<Favorite> findAllByUser(User user);
    Favorite findByUserAndProduct(User user, Product product);
}
