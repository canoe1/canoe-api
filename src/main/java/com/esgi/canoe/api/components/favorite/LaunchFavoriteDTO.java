package com.esgi.canoe.api.components.favorite;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class LaunchFavoriteDTO {
    private String type;
    private List<String> productUrls;
    private String token;


    @Override
    public String toString() {
        return "{\n" +
                "\"type\": \""+this.type+"\",\n" +
                "\"token\" : \""+token+"\",\n" +
                "\"enable_aliExpress\" : \""+false+"\",\n" +
                "\"product_urls\" : \""+String.join("$-_.+!*'(),",productUrls) +"\"\n" +
                "}";
    }
}
