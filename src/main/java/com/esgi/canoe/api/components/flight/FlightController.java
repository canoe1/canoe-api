package com.esgi.canoe.api.components.flight;

import com.esgi.canoe.api.components.broker.Runner;
import com.esgi.canoe.api.components.search.GetSearchDTO;
import com.esgi.canoe.api.components.search.Search;
import com.esgi.canoe.api.components.search.SearchRepository;
import com.esgi.canoe.api.components.search.SearchService;
import com.esgi.canoe.api.components.user.User;
import com.esgi.canoe.api.components.user.UserRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/flight")
public class FlightController {

    private final SearchRepository searchRepository;
    private final Runner runner;
    private final UserRepository userRepository;
    private final SearchService searchService;

    public FlightController(SearchRepository searchRepository, Runner runner, UserRepository userRepository, SearchService searchService) {
        this.searchRepository = searchRepository;
        this.runner = runner;
        this.userRepository = userRepository;
        this.searchService = searchService;
    }

    @PostMapping
    public ResponseEntity<Void> postFlight(@Validated @RequestBody PostFlightDTO postFlightDTO,
                                           @RequestHeader("Authorization") String authKey){
        String token = authKey.split(" ")[1];
        System.out.println(postFlightDTO.toString());

        Search previousSearch = this.searchRepository.findByTokenAndStatus(token, "ongoing");

        if(previousSearch != null){
            previousSearch.setStatus("cancelled");
            this.searchRepository.save(previousSearch);
        }

        try {
            this.runner.setJson(postFlightDTO.toStringAllParams(token));
            this.runner.runSearch();

            Search search = Search.builder()
                    .token(token)
                    .type(postFlightDTO.type)
                    .product_name(postFlightDTO.origin_airport+" to "+postFlightDTO.destination_airport+";departure"+postFlightDTO.departure_date)
                    .searchResult("")
                    .percentage(0)
                    .status("ongoing")
                    .file_path("")
                    .basket("")
                    .build();

            this.searchRepository.save(search);

            return ResponseEntity.accepted().build();
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        }
    }

    @GetMapping("/token")
    public GetSearchDTO getSearchFlight(@RequestHeader("Authorization") String authKey){
        String token = authKey.split(" ")[1];

        Search result = this.searchRepository.findByToken(token);
        GetSearchDTO resultDto = null;
        if (!result.getSearchResult().equals("")){
            resultDto = this.searchService.getSearchDTO(result);

            result.setSearchResult("");
            result.setToken("");

            this.searchRepository.save(result);
        }
        return resultDto;
    }
}
