package com.esgi.canoe.api.components.user;

import com.esgi.canoe.api.shared.services.URIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.annotation.*;
import org.springframework.web.bind.annotation.*;


import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {
    private final UserService userService;
    private final UserRepository userRepository;
    private final URIService uriService;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserController(UserService userService, UserRepository userRepository, URIService uriService, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.userRepository = userRepository;
        this.uriService = uriService;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping
    public List<GetUserDTO> getUsers() {
        return userService.getUsers();
    }

    @PostMapping
    public ResponseEntity<Void> CreateUser(@Validated @RequestBody CreateUserDTO user) {
        User u = User
                .builder()
                .email(user.getEmail())
                .password( passwordEncoder.encode(user.getPassword()))
                .firstname(user.getFirstname())
                .lastname(user.getLastname())
                .role("USER")
                .build();

        u = this.userRepository.save(u);

        URI location = this.uriService.getUser(u.getUserId());

        return ResponseEntity.created(location).build();
    }
}
