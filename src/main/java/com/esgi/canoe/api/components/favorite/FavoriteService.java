package com.esgi.canoe.api.components.favorite;


import com.esgi.canoe.api.components.broker.Runner;
import com.esgi.canoe.api.components.follow.Follow;
import com.esgi.canoe.api.components.product.Product;
import com.esgi.canoe.api.components.product.ProductRepository;
import com.esgi.canoe.api.components.search.Search;
import com.esgi.canoe.api.components.search.SearchRepository;
import com.esgi.canoe.api.components.user.User;
import com.esgi.canoe.api.components.user.UserRepository;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FavoriteService {
    private final UserRepository userRepository;
    private final ProductRepository productRepository;
    private final FavoriteRepository favoriteRepository;
    private final SearchRepository searchRepository;
    private final Runner runner;

    public FavoriteService(UserRepository userRepository, ProductRepository productRepository, FavoriteRepository favoriteRepository, SearchRepository searchRepository, Runner runner) {
        this.userRepository = userRepository;
        this.productRepository = productRepository;
        this.favoriteRepository = favoriteRepository;
        this.searchRepository = searchRepository;
        this.runner = runner;
    }

    public void postFavorite(PostFavoriteDTO postFavoriteDTO) {
        try {

            User user = this.userRepository.findByEmail(postFavoriteDTO.email)
                    .orElseThrow(() -> new AuthenticationServiceException("email " + postFavoriteDTO.email + " not found"));
            Product product = this.productRepository.findByUrl(postFavoriteDTO.product_url);

            if (product == null) {
                product = Product.builder()
                        .url(postFavoriteDTO.product_url)
                        .build();

                product = this.productRepository.save(product);
            }


            Favorite previousFavorite =  this.favoriteRepository.findByUserAndProduct(user, product);

            if(previousFavorite != null){
                previousFavorite.setRatings(postFavoriteDTO.rating);

                this.favoriteRepository.save(previousFavorite);
            }else{
                Favorite favorite = Favorite.builder()
                        .ratings(postFavoriteDTO.rating)
                        .user(user)
                        .product(product)
                        .build();

                this.favoriteRepository.save(favorite);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void launchGetFavoriteProcess(String token) throws Exception {
        User user = this.userRepository.findByToken(token)
                .orElseThrow(() -> new AuthenticationServiceException("token " + token + " not found"));


        List<Favorite> favorites = this.favoriteRepository.findAllByUser(user);


        List<String> productUrls = new ArrayList<>();
        for (Favorite favorite : favorites) {
            productUrls.add(favorite.product.getUrl());
        }


        LaunchFavoriteDTO launchFavoriteDTO =  LaunchFavoriteDTO.builder()
                                                .productUrls(productUrls)
                                                .token(token)
                                                .type("favorite")
                                                .build();
        System.out.println(launchFavoriteDTO.toString());

        this.runner.setJson(launchFavoriteDTO.toString());
        this.runner.runSearch();

        Search search = Search.builder()
                .token(token)
                .type("favorite")
                .product_name("")
                .searchResult("")
                .file_path("")
                .percentage(0)
                .basket("")
                .status("ongoing")
                .build();
        this.searchRepository.save(search);
    }
}
