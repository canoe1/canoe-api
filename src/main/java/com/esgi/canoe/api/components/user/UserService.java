package com.esgi.canoe.api.components.user;

import com.esgi.canoe.api.shared.services.ResponseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {
    private final UserRepository userRepository;
    private final ResponseService responseService;


    @Autowired
    public UserService(UserRepository userRepository, ResponseService responseService){
        this.userRepository = userRepository;
        this.responseService = responseService;
    }

    @Transactional(readOnly = true)
    public List<GetUserDTO> getUsers() {
        return this.userRepository
                .findAll()
                .stream()
                .map((this.responseService::getUserDTO))
                .collect(Collectors.toList());
    }
}
