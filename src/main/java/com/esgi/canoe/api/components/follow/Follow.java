package com.esgi.canoe.api.components.follow;

import com.esgi.canoe.api.components.product.Product;
import com.esgi.canoe.api.components.user.User;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Getter
@Setter
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name="follows")
public class Follow {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer followId;

    @ManyToOne
    @JoinColumn(name = "user_id")
    User user;

    @ManyToOne
    @JoinColumn(name = "product_id")
    Product product;

    @NotNull
    public Float price_notify;

    public Float last_price_notified;
}
