package com.esgi.canoe.api.components.follow;

import com.esgi.canoe.api.components.product.Product;
import com.esgi.canoe.api.components.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FollowRepository extends JpaRepository<Follow, Integer> {
    List<Follow> findAllByUser(User user);
    List<Follow> findAll();
    Follow findByUserAndProduct(User user, Product product);
}
