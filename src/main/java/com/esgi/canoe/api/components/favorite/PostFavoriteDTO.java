package com.esgi.canoe.api.components.favorite;

import com.esgi.canoe.api.components.product.Product;
import com.esgi.canoe.api.components.user.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class PostFavoriteDTO {

    String email;

    String product_url;

    Float rating;

    @Override
    public String toString() {
        return "PostFavoriteDTO{" +
                "email='" + email + '\'' +
                ", productUrl='" + product_url + '\'' +
                ", rating=" + rating +
                '}';
    }
}
