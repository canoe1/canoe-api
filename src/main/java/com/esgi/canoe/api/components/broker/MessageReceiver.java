package com.esgi.canoe.api.components.broker;

import com.esgi.canoe.api.components.follow.FollowSchedulerService;
import com.esgi.canoe.api.components.follow.FollowService;
import com.esgi.canoe.api.components.search.Search;
import com.esgi.canoe.api.components.search.SearchRepository;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
@RabbitListener(queues = "to_java_queue")
public class MessageReceiver {

    private final SearchRepository searchRepository;
    private final FollowSchedulerService followSchedulerService;

    public MessageReceiver(SearchRepository searchRepository, FollowSchedulerService followSchedulerService) {
        this.searchRepository = searchRepository;
        this.followSchedulerService = followSchedulerService;
    }

    @RabbitHandler
    public void receiveMessage(byte[] message) {
        try {
            JSONObject returnMessage = new JSONObject(new String(message));

            List<Search> cancelled = this.searchRepository.findAllByTokenAndStatus(returnMessage.get("token").toString(), "cancelled");

            if(cancelled.size() != 0){
                cancelled.get(0).setToken("");
                cancelled.get(0).setSearchResult("");
                this.searchRepository.save(cancelled.get(0));
                return;
            }

            Search search = this.searchRepository.findByTokenAndStatus(returnMessage.get("token").toString(), "ongoing");
            if(search == null){
                return;
            }
            search.setSearchResult(returnMessage.get("data").toString());
            search.setPercentage(100);
            search.setStatus("Done");

            this.searchRepository.save(search);

            if(returnMessage.get("token").toString().equals("schedulerCanoe")){
                this.followSchedulerService.notifyFollowingUser();
            }else{
                System.out.println("message received : " +new String(message));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
