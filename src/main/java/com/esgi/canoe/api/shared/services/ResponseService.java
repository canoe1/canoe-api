package com.esgi.canoe.api.shared.services;

import com.esgi.canoe.api.components.user.GetUserDTO;
import com.esgi.canoe.api.components.user.User;
import org.springframework.stereotype.Service;

import java.net.URI;

@Service
public class ResponseService {
    private final URIService uriService;

    public ResponseService(URIService uriService) {
        this.uriService = uriService;
    }

    public GetUserDTO getUserDTO (User u) {
        return GetUserDTO
                .builder()
                .email(u.getEmail())
                .password(u.getPassword())
                .firstname(u.getFirstname())
                .lastname(u.getLastname())
                .role(u.getRole())
                .build();
    }
}
