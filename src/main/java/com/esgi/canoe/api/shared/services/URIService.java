package com.esgi.canoe.api.shared.services;

import lombok.Data;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@Data
@Service
public class URIService {

    public URI getUsers() {
        return ServletUriComponentsBuilder
                .fromCurrentContextPath()
                .path("/users")
                .build()
                .toUri();
    }

    public URI getUser(Integer id){
        return ServletUriComponentsBuilder
                .fromCurrentContextPath()
                .path("/users/{id}")
                .buildAndExpand(id)
                .toUri();
    }
}
