FROM maven:3.6.3-jdk-13 as maven-builder
WORKDIR /workspace
COPY . /workspace/
RUN mvn package

FROM openjdk:11
WORKDIR /app
COPY --from=maven-builder /workspace/target /app
EXPOSE 8080
CMD ["sh", "-c","java   -DCANOE_PORT=$CANOE_PORT \
                        -DMYSQL_HOST=$MYSQL_HOST \
                        -DMYSQL_PORT=$MYSQL_PORT \
                        -DMYSQL_DATABASE=$MYSQL_DATABASE \
                        -DMYSQL_USERNAME=$MYSQL_USERNAME \
                        -DMYSQL_PASSWORD=$MYSQL_PASSWORD \
                        -DJWT_SECRET_KEYSECRET_KEY=$SECRET_KEY -jar *.jar"]
